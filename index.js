import './App/Config/ReactotronConfig'
import { AppRegistry, Platform } from 'react-native'
import App from './App/Containers/App'
import { Actions as NavigationActions } from 'react-native-router-flux'
import FCM, { FCMEvent, RemoteNotificationResult, WillPresentNotificationResult, NotificationType } from 'react-native-fcm';

FCM.on(FCMEvent.Notification, async (notif) => {
    if (notif.local_notification) {
      NavigationActions.gesture()
    }
    if (notif.opened_from_tray) {
      NavigationActions.gesture()
    }
    if (Platform.OS === 'ios') {
      switch (notif._notificationType) {
        case NotificationType.Remote:
          notif.finish(RemoteNotificationResult.NewData) //other types available: RemoteNotificationResult.NewData, RemoteNotificationResult.ResultFailed
          break;
        case NotificationType.NotificationResponse:
          notif.finish();
          break;
        case NotificationType.WillPresent:
          notif.finish(WillPresentNotificationResult.All) //other types available: WillPresentNotificationResult.None
          break;
      }
    }
  });

AppRegistry.registerComponent('asoV2', () => App)
