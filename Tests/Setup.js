// Mock your external modules here if needed
jest
.mock('react-native-cookies', () => {
  return { get: jest.fn((url) => { return [] }) }
})
.mock('react-native-router-flux', () => {
  return { Actions: { login: jest.fn(() => { return false }) } }
})

console.tron = { log: () => {}, display: () => {} }
