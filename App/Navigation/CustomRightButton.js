import React from 'react'
import Icon from 'react-native-vector-icons/FontAwesome'
import { TouchableOpacity } from 'react-native'

export default class CustomRightButton extends React.Component {
  constructor (props) {
    super(props)
  }
  render () {
    return (
      <TouchableOpacity onPress={this.props.navigateTo}>
      <Icon name={this.props.iconName} size={30} color="#FFFFFF" style={{ marginRight: 20, fontSize: 18}} />
     </TouchableOpacity>
    )
  }
}