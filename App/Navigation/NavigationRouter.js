import React, { Component } from 'react'
import { Button, Image, Alert } from 'react-native'
import { Drawer, Scene, Router, Stack, Footer, FooterTab } from 'react-native-router-flux'
import styles from './Styles/NavigationContainerStyles'
import DrawerContent from '../Containers/DrawerContent'
import CustomBackButton  from './CustomBackButton'
import CustomLeftButton from './CustomLeftButton'
import CustomRightButton from './CustomRightButton'

// screens identified by the router
import LaunchScreen from '../Containers/LaunchScreen'
import LoginScreen from '../Containers/LoginScreen'
import HomeScreen from '../Containers/HomeScreen'
import RegisterScreen from '../Containers/RegisterScreen'
import EntitiesScreen from '../Containers/EntitiesScreen'
import SettingsScreen from '../Containers/SettingsScreen'
import ChangePasswordScreen from '../Containers/ChangePasswordScreen'
import ForgotPasswordScreen from '../Containers/ForgotPasswordScreen'
import ShakeScreen from '../Containers/ShakeScreen'
import RestRewardScreen from '../Containers/RestRewardScreen'
import TeamGroupScreen from '../Containers/TeamGroupScreen'
import GestureScreen from '../Containers/GestureScreen'
import CreateGroupScreen from '../Containers/CreateGroupScreen'
import GroupDetailScreen from '../Containers/GroupDetailScreen'
import InviteNewMemberScreen from '../Containers/InviteNewMemberScreen'
import NotificationTest from '../Containers/NotificationTestScreen'
import ConfigureRestGroupScreen from '../Containers/ConfigureRestGroupScreen'
import { Actions as NavigationActions } from 'react-native-router-flux'
// ignite-jhipster-navigation-import-needle

/* **************************
* Documentation: https://github.com/aksonov/react-native-router-flux
***************************/

const openDrawer = () => {
  NavigationActions.drawerOpen()
}

class NavigationRouter extends Component {
  render () {
    return (
      <Router>
        <Scene drawer={true} key="drawer" headerTintColor={'white'} contentComponent={DrawerContent} navigationBarStyle={styles.navBar} titleStyle={styles.title} >
          <Stack key='root'>
            <Scene initial key='launchScreen' hideNavBar={true} component={LaunchScreen} title='Welcome' />
            <Scene key='homeScreen' component={HomeScreen} title='ASO'
           renderLeftButton={() => <CustomLeftButton onPress={openDrawer}/>} onLeft={openDrawer} />
            <Scene key='login' component={LoginScreen} titlea='Login' hideNavBar />
            <Scene key='register' component={RegisterScreen} title='Register' back />
            <Scene key='forgotPassword' component={ForgotPasswordScreen} title='Forgot Password' back />
            <Scene key='entities' component={EntitiesScreen} title='Entities' back />
            <Scene key='settings' component={SettingsScreen} title='Settings' back />
            <Scene key='changePassword' component={ChangePasswordScreen} title='Change Password' back />
            <Scene key='restReward' component={RestRewardScreen} title='Rest Reward' back/>
            <Scene key="gesture" component={GestureScreen} title="Rest Gesture"
              renderBackButton={() => <CustomBackButton/>} back />
            <Scene key='createGroup' component={CreateGroupScreen} title='Create Group'
              renderBackButton={() => <CustomBackButton/>} back />
            <Scene key='groupDetail' component={GroupDetailScreen} title='Group Detail'
              renderBackButton={() => <CustomBackButton/>} back />
            <Scene key='inviteNewMember' component={InviteNewMemberScreen} title='Invite New Member'
              renderBackButton={() => <CustomBackButton/>} back />
            <Scene key='notificationTest' component={NotificationTest} title='Task List'
              renderBackButton={() => <CustomBackButton/>} renderRightButton={() => <CustomRightButton iconName="home" navigateTo={NavigationActions.launchScreen}/>}
              back />
            <Scene key='configureRestGroup' component={ConfigureRestGroupScreen} title='Group Rest Settings'
              renderBackButton={() => <CustomBackButton/>} back />
            {/* ignite-jhipster-navigation-needle */}
          </Stack>
        </Scene>
      </Router>
    )
  }
}

export default NavigationRouter
