import React from 'react'
import { Alert, Image, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import { Actions as NavigationActions } from 'react-native-router-flux'


export default class CustomLeftButton extends React.Component {
  constructor(props){
    super(props)
  }
  render () {
    return (
      <TouchableOpacity onPress={this.props.onPress}>
        <Image width={35} height={35} source={require('../Images/aso_logo_white.png')} style={{width:35, height: 35, padding: 15, marginLeft: 20}} />
      </TouchableOpacity>
     
    )
  }
}