import React from 'react'
import { TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import { Actions as NavigationActions } from 'react-native-router-flux'

export default class CustomBackButton extends React.Component {

  render () {
    return (
      <TouchableOpacity onPress={NavigationActions.pop}>
       <Icon name="arrow-left" size={30} color="#FFFFFF" style={{ marginLeft: 20, fontSize: 18}} />
      </TouchableOpacity>
     
    )
  }
}