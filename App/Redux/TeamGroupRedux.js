import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'
import { data } from '../Data/TeamGroupDataSet';

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  teamGroupRequest: ['data'],
  teamGroupSuccess: ['payload'],
  teamGroupFailure: null,
  teamGroupSave: ['data'],
  teamGroupSetInit: null
})

export const TeamGroupTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: data,
  fetching: null,
  payload: null,
  error: null
})

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state, { data }) =>
  state.merge({ fetching: true, data, payload: null })

// successful api lookup
export const success = (state, action) => {
  const { payload } = action
  return state.merge({ fetching: false, error: null, payload })
}

// Something went wrong somewhere.
export const failure = state =>
  state.merge({ fetching: false, error: true, payload: null })

export const teamGroupSave = (state, { data }) =>
  state.merge({ fetching: true, data, payload: null })

export const teamGroupSetInit = state => INITIAL_STATE  

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.TEAM_GROUP_REQUEST]: request,
  [Types.TEAM_GROUP_SUCCESS]: success,
  [Types.TEAM_GROUP_FAILURE]: failure,
  [Types.TEAM_GROUP_SAVE]: teamGroupSave,
  [Types.TEAM_GROUP_SET_INIT]: teamGroupSetInit
})