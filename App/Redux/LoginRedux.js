import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'
/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  loginRequest: ['loginType', 'name', 'email', 'pp', 'accessToken'],
  loginSuccess: ['accessToken'],
  loginReset: [],
  loginFailure: ['error'],
  logoutRequest: null,
  logoutSuccess: null,
  logout: null
})

export const LoginTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  accessToken: null,
  name: null,
  pp: null,
  email: null,
  loginType: null,
  error: null,
  fetching: false,
  loading: false
})

/* ------------- Reducers ------------- */


export const loginRequest = (state, { loginType, name, email, pp, accessToken }) => {
  return state.merge({ loginType, name,email, pp, accessToken})
}
  

// we're attempting to login
export const request = (state) => state.merge({ fetching: true })

// we've successfully logged in
export const success = (state, data) => {
  const { accessToken } = data
  return state.merge({ fetching: false, error: null, accessToken })
}



// we've had a problem logging in
export const failure = (state, { error }) => state.merge({ fetching: false, error, accessToken: null })

// we need to logout, meaning clear access tokens and account
export const logoutRequest = state => INITIAL_STATE

export const loginReset = state => INITIAL_STATE
// we've logged out
export const logoutSuccess = state => INITIAL_STATE

export const logout = () => INITIAL_STATE

 
/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.LOGIN_REQUEST]: loginRequest,
  [Types.LOGIN_RESET]: loginReset,
  [Types.LOGOUT]: logout,
  [Types.LOGOUT_REQUEST]: logoutRequest,
  [Types.LOGOUT_SUCCESS]: logoutSuccess
  

})

/* ------------- Selectors ------------- */

export const isLoggedIn = (loginState) => loginState.accessToken !== null
