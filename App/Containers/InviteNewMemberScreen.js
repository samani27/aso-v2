import React from 'react'
import { ScrollView, KeyboardAvoidingView } from 'react-native'
import { Container, Content, List, ListItem, Left, Thumbnail, Right, Body, Text, Button, Icon } from 'native-base';
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/InviteNewMemberScreenStyle'

// Data
import { data } from '../Data/FriendListDataSet'

class InviteNewMemberScreenScreen extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      friends: data
    }
  }

  render () {
    return (
      <ScrollView style={styles.container}>
        <KeyboardAvoidingView behavior='position'>
          <Container>
            <Content>
              <List>
                {this.state.friends.map((item) => {
                  return (
                    <ListItem avatar key={item.index}>
                      <Left>
                        <Thumbnail source={item.image} />
                      </Left>
                      <Body>
                        <Text>{item.name}</Text>
                      </Body>
                      <Right>
                        <Button primary style={{marginBottom:10}}><Icon name='md-person-add' /></Button>
                      </Right>
                    </ListItem>
                  )
                })}
              </List>
            </Content>
          </Container>
        </KeyboardAvoidingView>
      </ScrollView>
    )
  }

}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(InviteNewMemberScreenScreen)
