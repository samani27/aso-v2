import React from 'react'
import { ScrollView, KeyboardAvoidingView } from 'react-native'
import { Container, Content, Thumbnail, Text, Button, List, ListItem, Left, Right, Body, Icon } from 'native-base'
import { connect } from 'react-redux'
import { Actions as NavigationActions } from 'react-native-router-flux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/GroupScreenStyle'

// Data
import { data } from '../Data/TeamMemberDataSet'

class GroupDetailScreenScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      members: data
    }
  }
  
  handlePressInviteNewMember = () => {
      NavigationActions.drawerClose()
      NavigationActions.inviteNewMember()
  }
  handlePressConfigure = () => {
      NavigationActions.drawerClose()
      NavigationActions.configureRestGroup()
  }

  render () {
    return (
      <ScrollView style={styles.container}>
        <KeyboardAvoidingView behavior='position'>
          <Container>
            <Content>
              <Button
                    onPress={this.handlePressConfigure}
                    rounded style={styles.floatButton} primary>
                    <Icon name='md-settings' style={{fontSize:30, padding:10}} />
                </Button>
              <Thumbnail large source={require('../Images/Mitrais.png')} style={{ alignSelf: "center" }} />
              <Text style={styles.title}>Mitrais</Text>
              <Text style={styles.description}>Group untuk project Mitrais</Text>
              <Button primary iconRight onPress={this.handlePressInviteNewMember}><Text> Invite new member </Text><Icon name='md-person-add' /></Button>              
              <List>
                {this.state.members.map((item) => {
                  return (
                    <ListItem avatar key={item.index}>
                      <Left>
                        <Thumbnail source={item.image} />
                      </Left>
                      <Body>
                        <Text>{item.name}</Text>
                      </Body>
                      <Right>
                        <Button danger style={{marginTop:10}} iconRight><Text>Remove</Text><Icon name='close' /></Button>
                      </Right>
                    </ListItem>
                  )
                })}
              </List>
            </Content>
          </Container>
        </KeyboardAvoidingView>
      </ScrollView>
    )
  }

}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(GroupDetailScreenScreen)
