import React from 'react'
import { Container, ScrollView, Text, KeyboardAvoidingView, ListItem, Button, Left, Right, Thumbnail, Body, List, Icon } from 'react-native'
import { connect } from 'react-redux'
import AsoTimelineList from '../Components/AsoTimelineList'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/TimelineScreenStyle'

class TimelineScreen extends React.Component {

  render () {
    return (
      <ScrollView style={styles.container}>
        <KeyboardAvoidingView behavior='position'>
          <AsoTimelineList />
        </KeyboardAvoidingView>
      </ScrollView>
    )
  }

}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(TimelineScreen)
