import React from 'react'
import { View, ScrollView, Text, KeyboardAvoidingView, Modal, Image } from 'react-native'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

import Button from 'react-native-button';

// Styles
import styles from './Styles/RestRewardScreenStyle'

class RestRewardScreenScreen extends React.Component {

  state = {
    modalVisible: false,
  }

  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }

  render () {
    return (
      <ScrollView style={styles.container}>
        <KeyboardAvoidingView behavior='position'>
          <View>
              <Button
                  onPress={() => { this.setModalVisible(true)}}
                  containerStyle={styles.buttonContainer} 
                  style={styles.button}
              >Trigger Reward</Button>

              <Modal
                  animationType="slide"
                  transparent={false}
                  visible={this.state.modalVisible}
                  onRequestClose={() => {this.setModalVisible(!this.state.modalVisible)}}
              >
                  <View style={styles.modalBody}>
                      <Image source={require('../Images/trophy.png')} style={{width: 100, height: 100}} />
                      <Text style={styles.modalContent}>Congratulations!{'\n'}You got 10 vitamins</Text>
                      <Text style={styles.modalContentSmall}>You did the rest right on the schedule{'\n'}Keep doing it to get more vitamins.</Text>

                      <Button 
                          onPress={() => {this.setModalVisible(!this.state.modalVisible)}}
                          containerStyle={styles.buttonContainer} 
                          style={styles.button}
                      >Close</Button>
                  </View>
              </Modal>
          </View>
        </KeyboardAvoidingView>
      </ScrollView>
    )
  }

}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(RestRewardScreenScreen)
