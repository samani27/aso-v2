import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { ScrollView, Image, BackHandler } from 'react-native'
import styles from './Styles/DrawerContentStyles'
import { Images } from '../Themes'
import DrawerButton from '../Components/DrawerButton'
import { Actions as NavigationActions } from 'react-native-router-flux'
import { connect } from 'react-redux'
import LoginActions from '../Redux/LoginRedux'

class DrawerContent extends Component {
  constructor(props){
    super(props)
    this.handlePressLogout = this.handlePressLogout.bind(this)
  }
  static propTypes = {
    dispatch: PropTypes.func,
    fetching: PropTypes.bool,
    attemptLogin: PropTypes.func
  }

  componentWillReceiveProps (newProps) {
    this.forceUpdate()
    if (newProps.accessToken === null) {
      NavigationActions.launchScreen()
    }
  }
  componentDidMount () {
    BackHandler.addEventListener('hardwareBackPress', () => {
      NavigationActions.drawerClose()
    })
  }

  handlePressLogin = () => {
    NavigationActions.drawerClose()
    NavigationActions.launchScreen()
  }
  handlePressRegister = () => {
    NavigationActions.drawerClose()
    NavigationActions.register()
  }
  handlePressForgotPassword = () => {
    NavigationActions.drawerClose()
    NavigationActions.forgotPassword()
  }
  handlePressEntities = () => {
    NavigationActions.drawerClose()
    NavigationActions.entities()
  }
  handlePressSettings = () => {
    NavigationActions.drawerClose()
    NavigationActions.settings()
  }
  handlePressChangePassword = () => {
    NavigationActions.drawerClose()
    NavigationActions.changePassword()
  }
  handlePressLogout(){
    NavigationActions.drawerClose()
    alert("Logout Successfully")
    NavigationActions.launchScreen()
    this.props.logout()
  }
  handlePressShake = () => {
    NavigationActions.drawerClose()
    NavigationActions.shakeScreen()
  }
  handlePressGesture = () => {
    NavigationActions.drawerClose()
    NavigationActions.gesture()
  }
  handleOpenNotificationPage = () => {
    NavigationActions.drawerClose()
    NavigationActions.notificationTest()
  }

  render () {
    return (
      <ScrollView style={styles.container}>
        <Image source={(this.props.loggedIn != null)?{uri:this.props.image}:Images.logoJhipster} style={styles.logo} />
        {!this.props.loggedIn && (<DrawerButton text='Login' onPress={this.handlePressLogin} />)}
        {this.props.loggedIn && (<DrawerButton text='Rest Gesture' onPress={this.handlePressGesture} />)}
        {this.props.loggedIn && (<DrawerButton text='Task List' onPress={this.handleOpenNotificationPage} />)}
        {this.props.loggedIn && (<DrawerButton text='Logout' onPress={this.handlePressLogout} />)}
      </ScrollView>
    )
  }
}

DrawerContent.contextTypes = {
  drawer: PropTypes.object
}


const mapStateToProps = (state) => {
  return {
    loggedIn: state.login.accessToken,
    name : state.login.name,
    image: state.login.pp
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    logout: () => dispatch(LoginActions.logout())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(DrawerContent)
