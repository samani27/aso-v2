import { StyleSheet } from 'react-native'
import { Metrics, ApplicationStyles, Colors } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    flex: 1,
    paddingTop: Metrics.navBarHeight,
    paddingBottom: Metrics.baseMargin,
    backgroundColor: '#FFFFFF'
  },
  logoContainer: {
    alignItems: 'center'
  },
  logo: {
    marginTop: Metrics.doubleSection,
    height: Metrics.images.logo,
    width: Metrics.images.logo,
    resizeMode: 'contain',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
  },
  centered: {
    alignItems: 'center'
  },
  welcome: {
    fontSize: 36,
    fontWeight: '100',
    textAlign: 'center',
    margin: 10,
    color: '#1F3A93'
  },
  instructions: {
      fontSize: 28,
      fontWeight: '600',
      textAlign: 'center',
      color: '#1F3A93'
  },
  linkContainer:{
    marginLeft: 20,
    marginRight: 20,
    marginTop: 10,
  },
  fbButton: {
    backgroundColor:"#3b5998", 
    borderWidth: 0,
    padding: 15
  },
  gButton: {
    backgroundColor:"#C0392B", 
    padding: 15, 
    borderWidth: 0
  },
  linkSmallContainer:{
    marginLeft: 30,
    marginRight: 30,
    marginTop: 10,
  },
  linkButton: {
    padding: 10, 
    backgroundColor:"#EFEFEF", 
    borderWidth: 0,
  },
  gButtonContainer:{
      margin: 10,
      padding: 10,
      backgroundColor: '#C0392B',
  },
  description:{
      fontSize: 16,
      color: '#1F3A93',
      textAlign: 'center',
      marginBottom: 20, 
      padding: 10,
      paddingLeft: 20,
      paddingRight: 20
  },
  small:{
    fontSize: 14,
    color: '#1F3A93',
    textAlign: 'center',
    padding: 10,
    paddingLeft: 20,
    paddingRight: 20
},
  buttonContainer:{
      padding: 20,
      borderRadius: 5,
      backgroundColor: '#22313F'
  },
  button:{
      fontSize: 22, 
      color: '#ffffff'
  }
})
