import { StyleSheet } from 'react-native'
import { ApplicationStyles } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  mycontainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#1F3A93',
  },
  buttonContainer:{
      padding: 20,
      borderRadius: 5,
      backgroundColor: '#22313F',
      margin: 50
  },
  button:{
      fontSize: 22, 
      color: '#ffffff'
  },
  modalBody:{
      flex: 1,
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center'
  },
  modalContent:{
      fontSize: 28,
      fontWeight: '600',
      textAlign: 'center',
      color: 'black'
  },
  modalContentSmall:{
      fontSize: 20,
      textAlign: 'center',
      color: 'black',
      marginBottom:20,
      marginTop:20
  }
})
