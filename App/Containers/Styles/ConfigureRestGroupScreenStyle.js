import { StyleSheet } from 'react-native'
import { ApplicationStyles } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  floatButton : {
    position: 'absolute',
    bottom: 90,
    right: 10,
    height: 80
  }
})
