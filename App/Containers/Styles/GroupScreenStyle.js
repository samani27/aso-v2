import { StyleSheet } from 'react-native'
import { ApplicationStyles } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    flex: 1,
    paddingTop: 20,
    paddingBottom: 10,
    backgroundColor: '#FFFFFF'
  },
  title: {
    fontSize: 36,
    fontWeight: '100',
    textAlign: 'center',
    color: '#1F3A93'
  },
  description:{
      fontSize: 16,
      color: '#1F3A93',
      textAlign: 'center',
      marginBottom: 10, 
      paddingLeft: 20,
      paddingRight: 20
  },
  floatButton : {
    position: 'absolute',
    top: 0,
    right: 10,
    height: 70
  }
})
