import React from 'react'
import { ScrollView, KeyboardAvoidingView } from 'react-native'
import { Container, Content, List, ListItem, Text, Button, Icon } from 'native-base'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/ConfigureRestGroupScreenStyle'

class ConfigureRestGroupScreenScreen extends React.Component {

  render () {
    var items = ['08:00','09:00','10:00','11:00','13:00','14:00','15:00'];
    return (
      <ScrollView style={styles.container}>
        <KeyboardAvoidingView behavior='position'>
          <Container>
            <Content>
              <List dataArray={items}
                renderRow={(item) =>
                  <ListItem>
                    <Text>{item}</Text>
                  </ListItem>
                }>
              </List>
            </Content>
            <Button
                rounded style={styles.floatButton} primary>
                <Icon name='add' style={{fontSize:50, padding:10}} />
            </Button>
          </Container>
        </KeyboardAvoidingView>
      </ScrollView>
    )
  }

}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ConfigureRestGroupScreenScreen)
