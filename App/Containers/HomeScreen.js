import React from 'react'
import { View, ScrollView, Text, KeyboardAvoidingView } from 'react-native'
import TimelineScreen from './TimelineScreen'
import TeamGroupScreen from './TeamGroupScreen'
import AsoHeaderTab from '../Components/AsoHeaderTab'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/HomeScreenStyle'

class HomeScreen extends React.Component {
 

  render () {
    const timelineHeading = {
      iconName: "clock-o",
      title: "Timeline",
      notification: "4"
    }
    const groupHeading = {
      iconName: "users",
      title: "Groups",
      notification: "2"
    }
    const tabHeadings = [ timelineHeading, groupHeading ]
    const timeline = <TimelineScreen/>
    const group = <TeamGroupScreen/>
    const tabContent = [timeline, group]
    return (
      <AsoHeaderTab tabHeadings={tabHeadings} tabContent={tabContent}/> 
    )
  }

}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen)
