import React from 'react'
import { ScrollView, Text, KeyboardAvoidingView } from 'react-native'
import { Container, Content, List, ListItem, Text, Button, Icon, Right, Button, Icon } from 'native-base'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/NotificationTestScreenStyle'

class NotificationTestScreenScreen extends React.Component {

  render () {
    var items = ['Task 1','Task 2','Task 3','Task 4','Task 5','Task 6','Task 7'];
    return (
      <ScrollView style={styles.container}>
        <KeyboardAvoidingView behavior='position'>
          <Container>
            <Content>
              <List dataArray={items}
                renderRow={(item) =>
                  <ListItem>
                    <Text>{item}</Text>
                    <Right>
                      <Button success>
                        <Icon name="check"/>
                      </Button>
                    </Right>
                  </ListItem>
                }>
              </List>
            </Content>
            <Button
                rounded style={styles.floatButton} primary>
                <Icon name='add' style={{fontSize:50, padding:10}} />
            </Button>
          </Container>
        </KeyboardAvoidingView>
      </ScrollView>
    )
  }

}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(NotificationTestScreenScreen)
