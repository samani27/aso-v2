import React from 'react'
import { Platform, TextInput, Alert, AsyncStorage } from 'react-native'
import { connect } from 'react-redux'
import { cloneDeep } from 'lodash'
import { Container, Content, Form, Item, Label, Input, Button, Text, View } from "native-base";
import { Actions as NavigationActions } from 'react-native-router-flux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
import TeamGroupActions from '../Redux/TeamGroupRedux'

// Styles
import styles from './Styles/CreateGroupScreenStyle'
import { data } from '../Data/TeamGroupDataSet'

//FCM
import FCM, { FCMEvent, RemoteNotificationResult, WillPresentNotificationResult, NotificationType } from 'react-native-fcm';

FCM.on(FCMEvent.Notification, async (notif) => {
    if (notif.local_notification) {
    }
    if (notif.opened_from_tray) {
    }
    if (Platform.OS === 'ios') {
      switch (notif._notificationType) {
        case NotificationType.Remote:
          notif.finish(RemoteNotificationResult.NewData) //other types available: RemoteNotificationResult.NewData, RemoteNotificationResult.ResultFailed
          break;
        case NotificationType.NotificationResponse:
          notif.finish();
          break;
        case NotificationType.WillPresent:
          notif.finish(WillPresentNotificationResult.All) //other types available: WillPresentNotificationResult.None
          break;
      }
    }
  });
  
  FCM.on(FCMEvent.RefreshToken, (token) => {
    console.log(token);
  });

class CreateGroupScreenScreen extends React.Component {

  constructor (props) {
    super(props)
    this.state = {
      groupName: '',
      groupDescription: '',
      warning: ''
    }
    this.isAttempting = false
  }

  componentDidMount() {
    FCM.requestPermissions().then(() => {
      console.log('granted');
    }).catch(() => {
      this.setState(prevState => {
        return { warning: 'Warning: notification permission rejected!' }
      })
    });
    FCM.getFCMToken().then(token => {
      console.log(token)
    });
    this.notificationListener = FCM.on(FCMEvent.Notification, async (notif) => {
      // optional, do some component related stuff
      Alert.alert('got notif!'); 
    });
    FCM.getInitialNotification().then(notif => {
      //handle notification
    });
  }

  componentWillUnmount() {
    // stop listening for events
    this.notificationListener.remove();
  }

  handlePressSave = () => {
    const { groupName, groupDescription } = this.state
    this.isAttempting = true
    // attempt a login - a saga is listening to pick it up from here.
    // this.props.attemptLogin(username, password)
    let list = cloneDeep(this.props.data);
    let newData = {
      index: 3,
      name: groupName,
      description: groupDescription,
      image: require('../Images/Hackatown.png')
    };
    list.push(newData);
    this.props.save(list);

    toggleLocalNotification();
    NavigationActions.pop();
  }
  handlePressCancel = () => {
    NavigationActions.pop()
  }

  handleChangeGroupName = (text) => {
    this.setState({ groupName: text })
  }

  handleChangeGroupDescription = (text) => {
    this.setState({ groupDescription: text })
  }

  render () {
    const { groupName, groupDescription } = this.state
    return (
          <Container>
            <Content padder style={{backgroundColor:'white'}}>
              <Form>
                  <Item floatingLabel>
                      <Label>Group Name</Label>
                      <Input
                        ref='groupName'
                        value={groupName}
                        keyboardType='default'
                        returnKeyType='next'
                        autoCapitalize='none'
                        autoCorrect={false}
                        onChangeText={this.handleChangeGroupName}
                        underlineColorAndroid='transparent' />
                  </Item>
                  <Item floatingLabel>
                      <Label>Description</Label>
                      <Input
                        ref='groupDescription'
                        value={groupDescription}
                        keyboardType='default'
                        returnKeyType='next'
                        autoCapitalize='none'
                        autoCorrect={false}
                        onChangeText={this.handleChangeGroupDescription}
                        underlineColorAndroid='transparent' />
                  </Item>
                  <View padder>
                    <Button full onPress={this.handlePressSave} primary>
                      <Text>Save</Text>
                    </Button>
                  </View>
              </Form>
            </Content>
          </Container>

          //   <View style={[styles.loginRow]}>
          //     <TouchableOpacity style={styles.loginButtonWrapper} onPress={this.handlePressSave}>
          //       <View style={styles.loginButton}>
          //         <Text style={styles.loginText}>Sign In</Text>
          //       </View>
          //     </TouchableOpacity>
          //     <TouchableOpacity style={styles.loginButtonWrapper} onPress={this.handlePressCancel}>
          //       <View style={styles.loginButton}>
          //         <Text style={styles.loginText}>Cancel</Text>
          //       </View>
          //     </TouchableOpacity>
          //   </View>
          // </View>
    )
  }

}

const mapStateToProps = (state) => {
  return {
    data: state.teamGroup.data
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    save: (data) => dispatch(TeamGroupActions.teamGroupSave(data))
  }
}

const toggleLocalNotification = () => {
  FCM.presentLocalNotification({
    id: "0",                           
    title: "Congratulations!",                     
    body: "You are rewarded by 20 points by creating your own group!",                   
    sound: "default",                                  
    priority: "high",                                   
    click_action: "MAIN",                             
    badge: 1,                                          
    number: 1,                                        
    ticker: "My Notification Ticker",                  
    auto_cancel: true,                                  
    large_icon: "trophy",                          
    icon: "ic_launcher",                               
    big_text: "20 points have been rewarded to you. Take a more look on what you can do with ASO!",     
    sub_text: "Notification",                      
    color: "red",                                       
    vibrate: 300,                                     
    //group: "group",                                    
    //picture: "https://google.png",                    
    ongoing: false,                                      
   // my_custom_data:'my_custom_field_value',            
    lights: true,
    show_in_foreground: true                
});
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateGroupScreenScreen)
