import React from 'react'
import { Platform, ScrollView, Text,View, KeyboardAvoidingView, Modal, Image  } from 'react-native'
import { connect } from 'react-redux'
import Button from 'react-native-button'
import { Actions as NavigationActions } from 'react-native-router-flux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/RestRewardScreenStyle'

//FCM
import FCM, { FCMEvent, RemoteNotificationResult, WillPresentNotificationResult, NotificationType } from 'react-native-fcm';

FCM.on(FCMEvent.Notification, async (notif) => {
    if (notif.local_notification) {
      NavigationActions.gesture()
    }
    if (notif.opened_from_tray) {
      NavigationActions.gesture()
    }
    if (Platform.OS === 'ios') {
      switch (notif._notificationType) {
        case NotificationType.Remote:
          notif.finish(RemoteNotificationResult.NewData) //other types available: RemoteNotificationResult.NewData, RemoteNotificationResult.ResultFailed
          break;
        case NotificationType.NotificationResponse:
          notif.finish();
          break;
        case NotificationType.WillPresent:
          notif.finish(WillPresentNotificationResult.All) //other types available: WillPresentNotificationResult.None
          break;
      }
    }
  });
  
  FCM.on(FCMEvent.RefreshToken, (token) => {
    console.log(token);
  });

class GestureScreen extends React.Component {
  constructor(props){
    super(props);
    toggleLocalNotification()
  }

  componentDidMount() {
    this.setModalVisible(true)
    FCM.requestPermissions().then(() => {
      console.log('granted');
    }).catch(() => {
      this.setState(prevState => {
        return { warning: 'Warning: notification permission rejected!' }
      })
    });
    FCM.getFCMToken().then(token => {
      console.log(token)
    });
    this.notificationListener = FCM.on(FCMEvent.Notification, async (notif) => {
      // optional, do some component related stuff
      Alert.alert('got notif!'); 
    });
    FCM.getInitialNotification().then(notif => {
      //handle notification
    });
  }

  componentWillUnmount() {
    // stop listening for events
    this.notificationListener.remove();
  }

  state = {
    modalVisible: false,
    modalRewardVisible: false
  }

  setModalVisible(visible) {
    this.setState({modalVisible: visible});
    setTimeout(function(){
      this.setState({modalVisible: false});
      this.setState({modalRewardVisible: true});
    }.bind(this), 15000);
  }

  dismissRewardModal() {
    this.setState({modalRewardVisible: false});
  }

  render () {
    return (
      <ScrollView style={styles.container}>
        <KeyboardAvoidingView behavior='position'>
          <View>
              <Button
                  onPress={() => { this.setModalVisible(true)}}
                  containerStyle={styles.buttonContainer} 
                  style={styles.button}
              >Rest Time</Button>

              <Modal
                  animationType="slide"
                  transparent={false}
                  visible={this.state.modalVisible}
                  onRequestClose={() => {this.setModalVisible(!this.state.modalVisible)}}
              >
                  <View style={styles.modalBody}>
                      <Image source={require('../Images/gesture.png')} style={{width: 100, height: 100}} />
                      <Text style={styles.modalContent}>Rest Time</Text>
                      <Text style={styles.modalContentSmall}>You should do this gesture to get a reward.</Text>
                  </View>
              </Modal>

              <Modal
                  animationType="slide"
                  transparent={false}
                  visible={this.state.modalRewardVisible}
                  onRequestClose={() => {this.setModalVisible(!this.state.modalRewardVisible)}}
              >
                  <View style={styles.modalBody}>
                      <Image source={require('../Images/trophy.png')} style={{width: 100, height: 100}} />
                      <Text style={styles.modalContent}>Congratulations!{'\n'}You got 10 vitamins</Text>
                      <Text style={styles.modalContentSmall}>You did the rest right on the schedule{'\n'}Keep doing it to get more vitamins.</Text>

                      <Button 
                          onPress={() => {this.dismissRewardModal()}}
                          containerStyle={styles.buttonContainer} 
                          style={styles.button}
                      >Close</Button>
                  </View>
              </Modal>
          </View>
        </KeyboardAvoidingView>
      </ScrollView>
    )
  }

}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

const toggleLocalNotification = () => {
  FCM.presentLocalNotification({
    id: "0",                           
    title: "Reminder",                     
    body: "You should take your rest break right now",                   
    sound: "default",                                  
    priority: "high",                                   
    click_action: "GESTURE",                             
    badge: 1,                                          
    number: 1,                                        
    ticker: "My Notification Ticker",                  
    auto_cancel: true,                                  
    large_icon: "workout",                          
    icon: "ic_launcher",                               
    big_text: "Move your phone along with designated way to keep you healthy and stay productive!",     
    sub_text: "Notification",                      
    color: "red",                                       
    vibrate: 300,                                     
    //group: "group",                                    
    //picture: "https://google.png",                    
    ongoing: false,                                      
   // my_custom_data:'my_custom_field_value',            
    lights: true,
    show_in_foreground: true                
});
}

export default connect(mapStateToProps, mapDispatchToProps)(GestureScreen)
