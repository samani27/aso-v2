import React from 'react'
import { connect } from 'react-redux'
import { Actions as NavigationActions } from 'react-native-router-flux'
import { Container, Button, Content, List, Text, Icon } from 'native-base'
// Add Actions - replace 'Your' with whatever your reducer is called :)
import TeamGroupActions from '../Redux/TeamGroupRedux'

import AsoItemList from "../Components/AsoItemList";

// Styles
import styles from './Styles/TeamGroupScreenStyle'

class TeamGroupScreenScreen extends React.Component {

    constructor(props){
        super(props);
        props.init();
    }

    handlePressCreateGroup = () => {
        NavigationActions.drawerClose()
        NavigationActions.createGroup()
    }

    render () {
        return (
            <Container>
                <Content padder>
                    <List>
                    {this.props.data.map((item) => {
                        return (
                        <AsoItemList key={item.index} source={item.image} name={item.name} description={item.description} />
                        )
                    })}
                    </List>
                </Content>
                <Button
                    onPress={this.handlePressCreateGroup}
                    rounded style={styles.floatButton} primary>
                    <Icon name='add' style={{fontSize:50, padding:10}} />
                </Button>
            </Container>
        )
    }

}

const mapStateToProps = (state) => {
  return {
    data: state.teamGroup.data
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    init: () => dispatch(TeamGroupActions.teamGroupSetInit())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(TeamGroupScreenScreen)
