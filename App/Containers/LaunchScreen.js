import React, {Component} from 'react'
import { ScrollView, Text, Image, View } from 'react-native'
import { Images } from '../Themes'
import { connect } from 'react-redux'
import LoginActions from '../Redux/LoginRedux'
import Button from 'react-native-button';
import { Actions as NavigationActions } from 'react-native-router-flux'
import Icon from 'react-native-vector-icons/FontAwesome';
const FBSDK = require('react-native-fbsdk')
const {
  AccessToken,
  LoginManager,
  GraphRequest,
  GraphRequestManager
} = FBSDK

// Styles
import styles from './Styles/LaunchScreenStyles'

class LaunchScreen extends Component {
  constructor (props) {
    
    super(props);
    if(props.accessToken != null){
      
      NavigationActions.homeScreen()
    }

    this.handleFacebookLogin = this.handleFacebookLogin.bind(this)
    }
    
    componentWillReceiveProps (newProps) {
      this.forceUpdate()
      if (newProps.accessToken) {
        NavigationActions.homeScreen()
      }
    }

  //test function for navigate to test page
  handleOpenNotificationPage = () => {
    NavigationActions.notificationTest()
  }

  handleOpenHome = () => {
    NavigationActions.homeScreen()
  };
 

    //login facebook action
    handleFacebookLogin () {
        LoginManager.logInWithReadPermissions(['public_profile', 'email']).then((result) => {
          
          if (result.isCancelled) {
          } else {
            AccessToken.getCurrentAccessToken().then(
              (data) => {
                const infoRequest = new GraphRequest(
                  '/me',
                  {
                    accessToken: data.accessToken,
                    parameters: {
                      fields: {
                        string: 'email,name,first_name,middle_name,last_name,picture'
                      }
                    }
                  }, (error, result) => {
                    if (error) {
                      alert('Error fetching data: ' + error.toString())
                    
                    } else {
                      alert('Login Successfully')
                      this.props.loginRequest('facebook', result.name, result.email, result.picture.data.url, data.accessToken );
                      
                    }
                  }
                );
        
                new GraphRequestManager().addRequest(infoRequest).start()
              }
        )
      }
    },
      function (error) {
        var errorStr = error.toString()
        if (errorStr.includes('INTERNET_DISCONNECTED')) {
          alert('Login fail with error: no internet connection')
        } else {
          alert('Login fail with error: ' + error)
        }
      }
    )
  }

      handleLogout(){
        LoginManager.logOut()
        // this.props.logout()
        alert('logout successfully')
      }

    _renderButtonLogin() {
      const accessToken = null;
      if (accessToken == 'undifined' || accessToken==null) {
          return (
            <View>
            <Button
            onPress={this.handleFacebookLogin}
            containerStyle={styles.fbButtonContainer} 
            style={styles.button}
            accessibilityLabel="Tap this to toggle registration"
          >LOGIN USING FACEBOOK</Button>

          <Button
            onPress={this.props.onSidemenuToggle}
            containerStyle={styles.gButtonContainer} 
            style={styles.button}
            accessibilityLabel="Tap this to toggle registration"
          >LOGIN USING GOOGLE</Button>
            </View>
          );
      } else {
          return (
            <View>
            <Button
              onPress={this.handleLogout}
              containerStyle={styles.fbButtonContainer} 
              style={styles.button}
              accessibilityLabel="Tap this to toggle registration"
            >LOGOUT</Button>
            </View>
          );
      }
  }


  render () {
    return (
      <View style={styles.mainContainer}>
        <ScrollView style={styles.container}>
          <View style={styles.logoContainer}>
          <Image style={styles.logo} width={100} height={100} source={require('../Images/aso_logo.png')} style={{width: 100, height: 100}} />
          </View>
          <Text style={styles.welcome}>
            ASO
          </Text>
          <Text style={styles.instructions}>
            Assistive Self-Organizing Application
          </Text>
          <Text style={styles.description}>
            Manage your tasks and keep balancing between your work and health
          </Text>
          <View style={styles.linkContainer}>
            <Icon.Button name="facebook"  color="#FFFFFF" backgroundColor="transparent" onPress={this.handleFacebookLogin} style={styles.fbButton}>
              Login with Facebook
            </Icon.Button>
          </View>
      
          <Text style={styles.small}>
            or
          </Text>
          <View style={styles.linkSmallContainer}>
            <Icon.Button name="envelope" size={12}  color="#1F3A93" backgroundColor="transparent" onPress={this.handleOpenHome} style={styles.linkButton}>
              Login using Email
            </Icon.Button>
          </View>
        </ScrollView>
      </View>
    )
  }
}


const mapStateToProps = (state) => {
  return {
    loggingIn: state.login.loggingIn,
    accessToken: state.login.accessToken,
    loginType: state.login.loginType,
    name: state.login.name,
    email: state.login.email,
    pp: state.login.pp
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    loginRequest: (loginType, name, email, pp, accessToken ) => dispatch(LoginActions.loginRequest(loginType, name, email, pp, accessToken )),
    logoutRequest: () => dispatch(LoginActions.logoutRequest())
    
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LaunchScreen)
