import React from 'react'
import { View, Text, Image } from 'react-native'
import { Card, CardItem, Left, Right, Body, Thumbnail, Icon, Button, Separator } from 'native-base'
import styles from './Styles/AsoTimelineListStyle'
import { data } from '../Data/TimelineDataSet'

export default class AsoTimelineList extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      listItems: data
    }
  }
  render() {
    const items = this.state.listItems.map((item, i) => {
      return (
        <Card key={i}>
          <CardItem  >
            <Left>
              <Thumbnail source={require('../Images/logo-jhipster2x.png')} />
              <Body>
                <Text style={{ fontWeight: '700' }}>{item.username}</Text>
                <Text note>{item.taskName}</Text>
              </Body>
            </Left>
          </CardItem>
          <CardItem cardBody>
            <Image source={item.image} style={{ height: 200, width: null, flex: 1 }} />
          </CardItem>
          <CardItem>
            <Left>
              <Button transparent>
                <Icon active name="thumbs-up" />
                <Text> {item.likes} Likes</Text>
              </Button>
            </Left>
            <Body>
              <Button transparent>
                <Icon active name="chatbubbles" />
                <Text> {item.comments} Comments</Text>
              </Button>
            </Body>
            <Right>
              <Text>{item.time}</Text>
            </Right>
          </CardItem>
        </Card>
      )
    });

    return (
      <View>{items}</View>
    );
  }
}

// // Prop type warnings
// AsoTimelineList.propTypes = {
//   someProperty: React.PropTypes.object,
//   someSetting: React.PropTypes.bool.isRequired
// }
//
// // Defaults for props
// AsoTimelineList.defaultProps = {
//   someSetting: false
// }
