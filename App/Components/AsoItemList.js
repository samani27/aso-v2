import React from 'react'
import styles from './Styles/AsoItemListStyle'
import { Actions as NavigationActions } from 'react-native-router-flux'
import { ListItem, Left, Body, Text, Thumbnail } from "native-base";

export default class AsoItemList extends React.Component {

  handlePressGroupItem = () => {
    NavigationActions.drawerClose()
    NavigationActions.groupDetail()
  }

  render () {
    return (
      <ListItem avatar style={{marginLeft: 0, paddingLeft: 17}} onPress={this.handlePressGroupItem}>
        <Left>
            <Thumbnail source={this.props.source} />
        </Left>
        <Body>
            <Text>{this.props.name}</Text>
            <Text note>{this.props.description}</Text>
        </Body>
      </ListItem>
    )
  }
}

// // Prop type warnings
// AsoItemList.propTypes = {
//   someProperty: React.PropTypes.object,
//   someSetting: React.PropTypes.bool.isRequired
// }
//
// // Defaults for props
// AsoItemList.defaultProps = {
//   someSetting: false
// }
