import React from 'react'
import { View, Text } from 'react-native'
import styles from './Styles/AsoHeaderTabStyle'
import { Container, Header, Content, Tab, Tabs, TabHeading, Badge } from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';

export default class AsoHeaderTab extends React.Component {
  constructor(props){
    super(props)
  }

  //props.tabHeadings = contains array of headings
  //props.tabContent = contains array of contents
  //both tabHeadings and tabContent must have the same length
  
  render () {
    return (
      <Container style={{height: 70}}>
        <Tabs style={{backgroundColor: '#EFEFEF'}} initialPage={0}>
        {this.props.tabContent.map((content, i) => {
          return (
          <Tab key={i} 
          heading={<TabHeading><Icon style={{ color: '#FFFFFF' }} name={this.props.tabHeadings[i].iconName}/>
          <Text style={{ color: '#FFFFFF' }}>{ ' ' + this.props.tabHeadings[i].title}</Text>
          <Badge info style={{ width: 20, height: 20}}>
            <Text style={{color: '#FFFFFF' }}>{this.props.tabHeadings[i].notification}</Text>
          </Badge></TabHeading>}>
            {content}
          </Tab>
          )
        })}
        </Tabs>
        </Container>
    )
  }
}

// // Prop type warnings
// AsoHeaderTab.propTypes = {
//   someProperty: React.PropTypes.object,
//   someSetting: React.PropTypes.bool.isRequired
// }
//
// // Defaults for props
// AsoHeaderTab.defaultProps = {
//   someSetting: false
// }
