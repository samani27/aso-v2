export const data = [
    {
        index: 1,
        name: 'Rian Apriansyah',
        image: require('../Images/Rian.jpg')
    },
    {
        index: 2,
        name: 'Samani',
        image: require('../Images/Samani.jpg')
    },
    {
        index: 3,
        name: 'Shulha Yahya',
        image: require('../Images/Shulha.jpg')
    },
    {
        index: 4,
        name: 'Theodorus Yoga Mahendraputra',
        image: require('../Images/Theo.jpg')
    },
    {
        index: 5,
        name: 'Tommy Nurwantoro',
        image: require('../Images/Tommy.jpg')
    },
];