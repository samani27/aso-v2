export const data = [
    {
        id:1,
        username:'Shulha',
        category:'rest-completed',
        taskName:'completed the rest',   
        time:'now',
        time_note:'over 3 minutes.',
        likes: 4,
        comments: 5, 
        image: require('../Images/timeline-pic.jpeg')
    },
    {
        id:1,
        username:'Sam',
        category:'rest-failed',
        taskName:'missed the rest', 
        time:'2m ago',
        time_note:'',
        likes: 4,
        comments: 5  ,
        image: require('../Images/timeline-pic-1.jpeg')        
    },
    {
        id:1,
        username:'Sam',
        category:'rest-failed',
        taskName:'missed the rest', 
        time:'2m ago',
        time_note:'',
        likes: 4,
        comments: 5  ,
        image: require('../Images/timeline-pic-2.jpeg')           
    },
    {
        id:1,
        username:'Sam',
        category:'rest-failed',
        taskName:'missed the rest', 
        time:'2m ago',
        time_note:'',
        likes: 4,
        comments: 5     ,
        image: require('../Images/timeline-pic.jpeg')         
    },
    {
        id:1,
        username:'Sam',
        category:'rest-failed',
        taskName:'missed the rest', 
        time:'2m ago',
        time_note:'',
        likes: 4,
        comments: 5         ,
        image: require('../Images/timeline-pic-1.jpeg') 
    },
    {
        id:1,
        username:'Sam',
        category:'rest-failed',
        taskName:'missed the rest', 
        time:'2m ago',
        time_note:'',
        likes: 4,
        comments: 5  ,
        image: require('../Images/timeline-pic-2.jpeg')        
    },
    {
        id:1,
        username:'Sam',
        category:'rest-failed',
        taskName:'missed the rest', 
        time:'2m ago',
        time_note:'',
        likes: 4,
        comments: 5   ,
        image: require('../Images/timeline-pic.jpeg')          
    },
    {
        id:1,
        username:'Theo',
        category:'rest-completed',
        taskName:'completed the rest ',  
        time:'2m ago',
        time_note:'on time',
        likes: 4,
        comments: 5   ,
        image: require('../Images/timeline-pic-1.jpeg')           
    },
    {
        id:1,
        username:'Shulha',
        category:'task-completed',
        taskName:'finished configuring emulator',   
        time:'3m ago',
        time_note:'24m ahead of schedule',
        likes: 4,
        comments: 5     ,
        image: require('../Images/timeline-pic-2.jpeg')   
    },
    {
        id:2,
        username:'Sam',
        category:'task-completed',
        taskName:'finished building FB login feature',             
        time:'8m ago',
        time_note:'29m ahead of schedule',
        likes: 4,
        comments: 5 ,
        image: require('../Images/timeline-pic.jpeg')  
    },
    {
        id:1,
        username:'Theo',
        category:'task-completed',
        taskName:'building timeline',              
        time:'9m ago',
        time_note:'30m ahead of schedule',
        likes: 4,
        comments: 5,
        image: require('../Images/timeline-pic-1.jpeg')
    },
  ]