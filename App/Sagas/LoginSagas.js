import { call, put } from 'redux-saga/effects'
import LoginActions from '../Redux/LoginRedux'
import AccountActions from '../Redux/AccountRedux'

// attempts to login
export function * login (api, { username, password }) {
  const authObj = 'j_username=' + username + '&j_password=' + password + '&remember-me=true'

  const response = yield call(api.login, authObj)

  // success?
  if (response.ok) {
    yield put(LoginActions.loginSuccess(response.data))
    yield put(AccountActions.accountRequest())
    yield put({ type: 'RELOGIN_OK' })
  } else {
    yield put(LoginActions.loginFailure('WRONG'))
  }
}

// attempts to logout
export function * logout (api) {
  yield call(api.logout)
  yield put(AccountActions.accountRequest())
  yield put(LoginActions.logoutSuccess())
  yield put({ type: 'RELOGIN_ABORT' })
}
